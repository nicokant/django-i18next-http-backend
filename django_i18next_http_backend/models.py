from django import dispatch
from django.db import models
from django.db.models.signals import post_save


class Language(models.Model):
    code = models.CharField(max_length=4, unique=True)
    name = models.CharField(max_length=500)

    def __str__(self):
        return f'{self.name} {self.code}'


class Namespace(models.Model):
    name = models.CharField(max_length=200)


class Placeholder(models.Model):
    namespace = models.ForeignKey(Namespace, on_delete=models.CASCADE, null=True, blank=True)
    text = models.TextField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text


class Translation(models.Model):
    placeholder = models.ForeignKey(Placeholder, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    missing = models.BooleanField(default=True)

    def __str__(self):
        return self.text

    class Meta:
        unique_together = ['language', 'placeholder']


@dispatch.receiver(post_save, sender=Translation)
def update_missing_field(sender, instance, *args, **kwargs):
    if instance.text == '':
        instance.missing = True
    else:
        instance.missing = False
