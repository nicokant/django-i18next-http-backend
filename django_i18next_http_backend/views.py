from django.http import JsonResponse
from django.views import View

from django_i18next_http_backend import models


class TranslationsView(View):
    def get(self, request, language, namespace):
        if namespace == 'translations':
            namespace = None

        lang, create = models.Language.objects.get_or_create(code=language)
        translations = models.Translation.objects.filter(language=lang, placeholder_namespace=namespace, missing=False)
        translations_dict = {[t.placeholder.text]: t.text for t in translations}
        return JsonResponse(translations_dict)

    def post(self, request, language, namespace):
        lang, _created = models.Language.objects.get_or_create(code=language)
        ns, _created = models.Namespace.objects.get_or_create(name=namespace)
        for key, value in request.POST.items():
            p, _created =models.Placeholder.objects.get_or_create(text=key, namespace=ns)
            models.Translation.objects.get_or_create(language=lang, placeholder=p, text=value)

        return JsonResponse({'success': True})
